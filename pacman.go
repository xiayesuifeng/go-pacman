package pacman

import (
	"github.com/Jguer/go-alpm/v2"
	"github.com/Morganamilo/go-pacmanconf"
)

type Pacman struct {
	handle *alpm.Handle

	config *pacmanconf.Config
}

func NewPacman() (*Pacman, error) {
	config, _, err := pacmanconf.ParseFile("/etc/pacman.conf")
	if err != nil {
		return nil, err
	}

	handle, err := CreateHandle(config)
	if err != nil {
		return nil, err
	}

	return &Pacman{handle: handle, config: config}, nil

}

func CreateHandle(conf *pacmanconf.Config) (*alpm.Handle, error) {
	h, err := alpm.Initialize(conf.RootDir, conf.DBPath)
	if err != nil {
		return nil, err
	}
	for _, repo := range conf.Repos {
		db, err := h.RegisterSyncDB(repo.Name, 0)
		if err == nil {
			db.SetServers(repo.Servers)
		}
	}
	return h, nil
}

func (p *Pacman) SyncDB() (alpm.IDBList, error) {
	return p.handle.SyncDBs()
}

func (p *Pacman) Release() error {
	return p.handle.Release()
}
