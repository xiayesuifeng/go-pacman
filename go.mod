module gitlab.com/xiayesuifeng/go-pacman

go 1.16

require (
	github.com/Jguer/go-alpm/v2 v2.0.5
	github.com/Morganamilo/go-pacmanconf v0.0.0-20180910220353-9c5265e1b14f
)
